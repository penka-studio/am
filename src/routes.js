export const ROUTES = {
  INDEX: "/",
  PACKS: "/packs",
  PACKS_ID: "/packs/:id"
};
