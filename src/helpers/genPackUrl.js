import { ROUTES } from "../routes";

export const genPackUrl = (id) => `${ROUTES.PACKS}/${id}`;
