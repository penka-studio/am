import React from "react";

import "./styles.scss";
import { Typography } from "@material-ui/core";
import { AddPack } from "../../components/AddPack/component";
import { PacksList } from "../../components/PacksList/component";

export const Packs = (props) => {
    const { test } = props;

    return (
        <div className="test">
            <Typography gutterBottom variant="h4" component="h2">
                Мои паки
            </Typography>
            <AddPack/>
            <PacksList/>
        </div>
    )
};
