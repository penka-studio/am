import React from "react";

import { PacksList } from "../../components/PacksList/component";

import "./styles.scss";
import { PacksFilters } from "../../components/PacksFilters/component";
import { Typography } from "@material-ui/core";

export const Index = () => {
    return (
        <div className="index">
            <Typography gutterBottom variant="h4" component="h2">
                Все паки
            </Typography>
            <PacksFilters/>
            <PacksList />
        </div>
    )
};
