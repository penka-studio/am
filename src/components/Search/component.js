import React from "react";

import "./styles.scss";
import { InputBase, IconButton  } from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex'
    },
    input: {
        flex: 1,
    },
    iconButton: {
        padding: 6,
    },
}));

export const Search = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <InputBase
                className={classes.input}
                placeholder="Поиск по пакам"
            />
            <IconButton type="submit" className={classes.iconButton} aria-label="search">
                <SearchIcon />
            </IconButton>
        </div>
    )
};
