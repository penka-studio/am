import React from "react";

import { Search } from "../Search/component";

import "./styles.scss";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: '20px',
    },
}));

export const PacksFilters = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Search />
        </div>
    )
};
