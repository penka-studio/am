import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import "./styles.scss";
import { packs } from "./packs";
import { Card, CardContent, CardActionArea, Typography } from "@material-ui/core";
import {
    useHistory,
} from "react-router-dom";
import { genPackUrl } from "../../helpers/genPackUrl";

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
    },
}));

export const PacksList = (props) => {
    const history = useHistory();

    const classes = useStyles();

    const showAchievements = (id) => {
        history.push(genPackUrl(id));
    };

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                {
                    packs.map(pack => (
                        <Grid key={pack.id} item xs={12} sm={6}>
                            <Card className={classes.card}>
                                <CardActionArea onClick={() => showAchievements(pack.id)}>
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {pack.title}
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            {pack.description}
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                    ))
                }
            </Grid>
        </div>
    )
};
