export const packs = [
    {
        id: 1,
        title: "Заголовок 1 пака",
        description: "Описание 1 пака"
    },
    {
        id: 2,
        title: "Заголовок 2 пака",
        description: "Описание 2 пака",
    },
    {
        id: 3,
        title: "Заголовок 3 пака",
        description: "Описание 3 пака"
    },
    {
        id: 4,
        title: "Заголовок 4 пака",
        description: "Описание 4 пака"
    },
    {
        id: 5,
        title: "Заголовок 5 пака",
        description: "Описание 5 пака"
    },
    {
        id: 6,
        title: "Заголовок 6 пака",
        description: "Описание 6 пака"
    },
    {
        id: 7,
        title: "Заголовок 7 пака",
        description: "Описание 7 пака"
    },
    {
        id: 8,
        title: "Заголовок 8 пака",
        description: "Описание 8 пака"
    },
];
