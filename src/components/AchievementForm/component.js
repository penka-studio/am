import React, { useEffect, useRef, useState } from "react";
import { useStyles } from "./styles";
import {
    Input,
    InputLabel,
    FormControl,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText, DialogActions, Button
} from "@material-ui/core";

export const AchievementForm = (props) => {
    const classes = useStyles();

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <FormControl className={classes.formItem}>
                <InputLabel htmlFor="my-input">Имя пака</InputLabel>
                <Input id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
            <FormControl className={classes.formItem}>
                <InputLabel htmlFor="my-input">Описание</InputLabel>
                <Input id="my-input" aria-describedby="my-helper-text" />
            </FormControl>
        </form>
    )
};


// const [open, setOpen] = useState(false);
//
// const handleModalClose = () => {
//     setOpen(false);
// };
//
// const handleModalOpen = () => {
//     setOpen(true);
// };

// const descriptionElementRef = useRef(null);
// useEffect(() => {
//     if (open) {
//         const { current: descriptionElement } = descriptionElementRef;
//         if (descriptionElement !== null) {
//             descriptionElement.focus();
//         }
//     }
// }, [open]);


// <Dialog
//     open={open}
//     onClose={handleModalClose}
//     aria-labelledby="scroll-dialog-title"
//     aria-describedby="scroll-dialog-description"
// >
//     <DialogTitle id="scroll-dialog-title">Новый пак</DialogTitle>
//     <DialogContent dividers>
//         <DialogContentText
//             id="scroll-dialog-description"
//             ref={descriptionElementRef}
//             tabIndex={-1}
//         >
//         </DialogContentText>
//     </DialogContent>
//     <DialogActions>
//         <Button onClick={handleModalClose} color="primary">
//             Отмена
//         </Button>
//         <Button onClick={handleAddPack} color="primary">
//             Создать
//         </Button>
//     </DialogActions>
// </Dialog>
