import React, { useState } from "react";
import { Button, Collapse, Paper, TextField, IconButton, ClickAwayListener } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';

import { useStyles } from "./styles";

export const AddPack = () => {
    const classes = useStyles();
    const [checked, setChecked] = useState(false);

    const handleOpenForm = () => setChecked(true);
    const handleCloseForm = () => setChecked(false);

    const handleAddPack = () => {
    };

    return (
        <ClickAwayListener onClickAway={handleCloseForm}>
            <div className={classes.root}>
                <Button
                    className={classes.btn}
                    onClick={handleOpenForm}
                    color="secondary"
                    variant="contained"
                    startIcon={<AddIcon/>}
                >
                    Добавить новый пак
                </Button>
                <div className={classes.container}>
                    <Collapse in={checked}>

                        <Paper className={classes.paper}>
                            <TextField margin="dense" className={classes.formItem} id="name" label="Название"
                                       variant="outlined"/>
                            <TextField margin="dense" className={classes.formItem} id="description" label="Описание"
                                       variant="outlined"/>
                            <div className={classes.actionBtns}>
                                <Button onClick={handleAddPack} size="small" color="primary"
                                        variant="contained">Создать</Button>
                                <IconButton onClick={handleCloseForm} aria-label="delete" className={classes.close}>
                                    <CloseIcon fontSize="small"/>
                                </IconButton>
                            </div>
                        </Paper>
                    </Collapse>
                </div>
            </div>
        </ClickAwayListener>
    )
};
