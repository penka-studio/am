import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => ({
    root: {
        position: "relative",
        display: "inline-flex"
    },
    btn: {
        marginBottom: 20
    },
    container: {
        position: "absolute",
        top: 0,
        left: 0,
        zIndex: 3
    },
    paper: {
        padding: 10
    },
    formItem: {
        marginBottom: 10
    },
    actionBtns: {
        display: "flex"
    },
    close: {
        marginLeft: 7,
        padding: 5
    }
}));
