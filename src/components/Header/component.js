import React, { useState } from "react";

import "./styles.scss";
import { Menu, MenuItem, Button, Paper, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ROUTES } from "../../routes";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 5,
        marginBottom: 10,
        // borderBottomLeftRadius: 15,
        // borderBottomRightRadius: 15
    },
    login: {

    },
    logo: {
      marginBottom: 0
    }
}));

export const Header = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const classes = useStyles();

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const moveToPacks = () => {
        handleClose();
    };

    const logout = () => {
        console.log("exit from account!");
        handleClose();
    };

    return (
        <Paper className={classes.root}>
            <Link to={ROUTES.INDEX} className="logo">
                <Typography className={classes.logo} variant="h6" component="h1">
                    Achievements maker
                </Typography>
            </Link>
            <Button
                className={classes.login}
                aria-haspopup="true"
                onClick={handleClick}
                color="secondary"
                variant="contained"
            >
                Профиль
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}>
                    <Link to={ROUTES.PACKS}>
                        Мои паки
                    </Link>
                </MenuItem>
                <MenuItem onClick={logout}>Выход</MenuItem>
            </Menu>
        </Paper>
    )
};

// import { FirebaseContext } from '../Firebase/component';
// <FirebaseContext.Consumer>
//     {firebase => {
//         return <div>I've access to Firebase and render something.</div>;
//     }}
// </FirebaseContext.Consumer>
