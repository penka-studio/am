import React from 'react';
import { BrowserRouter as Router, Route, Switch, } from "react-router-dom";
import { palette, spacing, typography } from '@material-ui/system';
import { Container, createMuiTheme, Paper } from "@material-ui/core";
import styled, { ThemeProvider } from 'styled-components';

import Firebase, { FirebaseContext } from './components/Firebase/component';
import { Index } from "./pages/Index/component";
import { Packs } from "./pages/Packs/component";
import { ROUTES } from "./routes";
import { PacksView } from "./pages/PacksView/component";
import { Header } from "./components/Header/component";

import "./App.scss";
import { deepPurple, indigo, pink } from "@material-ui/core/colors";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: indigo[400]
        },
        secondary: pink
    },
});

const Box = styled.div`${palette}${spacing}${typography}`;

function App() {
    return (
        <FirebaseContext.Provider value={new Firebase()}>
            <Router>
                <ThemeProvider theme={theme}>
                    <Box
                        className="App"
                        bgcolor={theme.palette.primary.main}
                    >
                        <Container
                            maxWidth="lg"
                        >
                            <Header/>
                            <Paper
                                className="content"
                            >
                                <Switch>
                                    <Route exact path={ROUTES.INDEX} component={Index}/>
                                    <Route exact path={ROUTES.PACKS} component={Packs}/>
                                    <Route exact path={ROUTES.PACKS_ID} component={PacksView}/>
                                </Switch>
                            </Paper>
                        </Container>
                    </Box>
                </ThemeProvider>
            </Router>
        </FirebaseContext.Provider>
    );
}

export default App;
